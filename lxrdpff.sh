sudo add-apt-repository ppa:mozillateam/ppa

echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001

Package: firefox
Pin: version 1:1snap1-0ubuntu2
Pin-Priority: -1
' | sudo tee /etc/apt/preferences.d/mozilla-firefox


sudo snap remove firefox

sudo apt install firefox

sleep 5

firefox -CreateProfile 1
firefox -CreateProfile 2
firefox -CreateProfile 3
